import importlib
import os
import sys
import tempfile
import unittest
from unittest.mock import patch
from dependency_management.requirements.PipRequirement import (
    PipRequirement)
from coala_utils.ContextManagers import (
    retrieve_stdout, retrieve_stderr)
from coala_package_manager.installation_tool import (
    __doc__, get_output, install_requirements, check_requirements,
    get_all_bears_names_from_PyPI, install_bears,
    show_installed_bears, main)


class RunCommandTest(unittest.TestCase):

    def test_echo_command(self):
        self.test_command = ['echo', '"Hello World"']
        self.output = get_output(self.test_command)
        self.assertIn('Hello World', self.output)


class GetAllBearsTest(unittest.TestCase):

    def setUp(self):
        self.all_bears = get_all_bears_names_from_PyPI()

    def test_individual_bears(self):
        self.assertIsInstance(self.all_bears, list)
        self.assertIn('PyLintBear', self.all_bears)
        self.assertIn('PEP8Bear', self.all_bears)
        self.assertGreater(len(self.all_bears), 50)
        for bear_name in self.all_bears:
            self.assertRegex(bear_name, r'.+Bear')


class InstallRequirementsTest(unittest.TestCase):

    def setUp(self):
        PipRequirement('PEP8Bear').install_package()
        PipRequirement('PyLintBear').install_package()

    def test_requirements_installed(self):
        with retrieve_stdout() as stdout:
            self.assertEqual(install_requirements('PEP8Bear'), [])
            self.assertIn("(PEP8Bear's dependency) is already installed",
                          stdout.getvalue())

    def test_requirements_uninstalled(self):
        PipRequirement('pylint').uninstall_package()
        with retrieve_stdout() as stdout:
            self.assertEqual(install_requirements('PyLintBear'), [])
            self.assertIn('is installing...',
                          stdout.getvalue())
            self.assertIn('DONE', stdout.getvalue())

    @patch.object(PipRequirement, 'install_package')
    def test_failing_requirement(self, mock_method):
        mock_method.return_value = 1
        PipRequirement('pylint').uninstall_package()
        with retrieve_stdout() as stdout:
            self.assertEqual(install_requirements('PyLintBear'),
                             ['PyLintBear'])
            self.assertIn('FAILED!', stdout.getvalue())

    def test_fail_is_installed_requirement(self):
        package_object = importlib.import_module('coalaPyLintBear.PyLintBear')
        functions = []
        for requirement in getattr(package_object, 'PyLintBear').REQUIREMENTS:
            functions.append(requirement.is_installed)
            requirement.is_installed = PipRequirement.is_installed

        with patch.object(PipRequirement, 'is_installed') as mock_method:
            mock_method.side_effect = Exception('Something Bad')
            with retrieve_stdout() as stdout:
                self.assertEqual(install_requirements('PyLintBear'),
                                 [])
                self.assertIn('FAILED!', stdout.getvalue())
                self.assertIn('is not installed. Cannot install dependencies',
                              stdout.getvalue())
        for idx, requirement in enumerate(getattr(package_object,
                                                  'PyLintBear').REQUIREMENTS):
            requirement.is_installed = functions[idx]

    @patch.object(PipRequirement, 'install_package')
    def test_exception_installing_bear(self, mock_method):
        mock_method.side_effect = Exception('Something bad')
        PipRequirement('pylint').uninstall_package()
        with retrieve_stdout() as stdout:
            self.assertEqual(install_requirements('PyLintBear'),
                             ['PyLintBear'])
            self.assertNotIn('FAILED!', stdout.getvalue())
            self.assertNotIn('DONE', stdout.getvalue())

    def test_not_installed_bear(self):
        try:
            install_requirements('PyLintBear')
        except ImportError as err:
            self.assertIn("No module named 'coalaPyLintBear'",
                          str(err))

    def tearDown(self):
        PipRequirement('PEP8Bear').uninstall_package()
        PipRequirement('PyLintBear').uninstall_package()
        PipRequirement('pylint').uninstall_package()


class CheckRequirementsTest(unittest.TestCase):

    def setUp(self):
        PipRequirement('PEP8Bear').install_package()
        PipRequirement('VintBear').install_package()

    def test_installed_requirements(self):
        with retrieve_stdout() as stdout:
            check_requirements('PEP8Bear')
            self.assertIn('autopep8 is installed.\n', stdout.getvalue())

    def test_requirement_not_installed(self):
        PipRequirement('vim-vint').uninstall_package()
        with retrieve_stdout() as stdout:
            check_requirements('VintBear')
            self.assertIn('vim-vint is not installed.\n', stdout.getvalue())

    def test_bear_not_installed(self):
        with retrieve_stdout() as stdout:
            self.assertEqual(check_requirements('coalaBear'), 1)
            self.assertIn('coalaBear has missing dependencies.\n',
                          stdout.getvalue())

    def tearDown(self):
        PipRequirement('PEP8Bear').uninstall_package()
        PipRequirement('VintBear').uninstall_package()


class InstallBearsTest(unittest.TestCase):

    def setUp(self):
        PipRequirement('PEP8Bear').install_package()
        PipRequirement('PyLintBear').install_package()

    def test_installed_bear(self):
        with retrieve_stdout() as stdout:
            self.assertEqual(install_bears(['PEP8Bear', 'PyLintBear'],
                                           True), [])
            self.assertIn('PEP8Bear is already installed.', stdout.getvalue())
            self.assertIn('PyLintBear is already installed.', stdout.getvalue())

    def test_installed_bear_with_deps(self):
        with retrieve_stdout() as stdout:
            self.assertEqual(install_bears(['PEP8Bear', 'PyLintBear'],
                                           False), [])
            self.assertIn('PEP8Bear is already installed.', stdout.getvalue())
            self.assertIn('PyLintBear is already installed.', stdout.getvalue())

    def test_install_bear(self):
        PipRequirement('PyLintBear').uninstall_package()
        with retrieve_stdout() as stdout:
            self.assertEqual(install_bears(['PyLintBear'], True), [])
            self.assertIn('DONE!', stdout.getvalue())

    def test_install_bear_with_deps(self):
        PipRequirement('PyLintBear').uninstall_package()
        with retrieve_stdout() as stdout:
            self.assertEqual(install_bears(['PyLintBear'], False), [])
            self.assertIn('DONE!', stdout.getvalue())

    @patch.object(PipRequirement, 'install_package')
    def test_fail_installing_bear(self, mock_method):
        mock_method.return_value = 1
        with retrieve_stdout() as stdout:
            self.assertEqual(install_bears(['VintBear'], False), ['VintBear'])
            self.assertIn('FAILED!', stdout.getvalue())

    @patch.object(PipRequirement, 'install_package')
    def test_exception_installing_bear(self, mock_method):
        mock_method.side_effect = Exception('Something bad')
        PipRequirement('pylint').uninstall_package()
        with retrieve_stdout() as stdout:
            self.assertEqual(install_bears(['VintBear'], False), ['VintBear'])
            self.assertNotIn('FAILED!', stdout.getvalue())
            self.assertNotIn('DONE', stdout.getvalue())

    def tearDown(self):
        PipRequirement('PEP8Bear').uninstall_package()
        PipRequirement('PyLintBear').uninstall_package()


class ShowInstalledBearsTest(unittest.TestCase):

    def setUp(self):
        self.installed_bears = ['PEP8Bear', 'PyLintBear']
        self.not_all_installed_bears = ['PEP8Bear', 'PyLintBear', 'TestBear']
        for bear in self.installed_bears:
            PipRequirement(bear).install_package()

    def test_all_bears_installed(self):
        self.assertTrue(show_installed_bears(self.installed_bears))

    def test_not_all_bears_installed(self):
        self.assertFalse(show_installed_bears(self.not_all_installed_bears))

    def tearDown(self):
        PipRequirement('PEP8Bear').uninstall_package()
        PipRequirement('PyLintBear').uninstall_package()


class TestWithInstalledCoalaBears(unittest.TestCase):

    def setUp(self):
        self.argv = ['installation_tool.py']
        argv_patcher = patch.object(sys, 'argv', self.argv)
        self.addCleanup(argv_patcher.stop)
        self.argv_mock = argv_patcher.start()

    def test_coala_bears_installed(self):
        PipRequirement('coala-bears').install_package()
        self.argv += ['install', 'all']
        with self.assertRaises(SystemExit) as cm:
            main()
            self.assertEqual(cm.exception.code, 1)

    def tearDown(self):
        PipRequirement('coala-bears').uninstall_package()


class MainTest(unittest.TestCase):

    def setUp(self):
        self.all_bears = get_all_bears_names_from_PyPI()
        self.argv = ['installation_tool.py']
        argv_patcher = patch.object(sys, 'argv', self.argv)
        self.addCleanup(argv_patcher.stop)
        self.argv_mock = argv_patcher.start()

    def tearDown(self):
        # Uninstall any installed bear
        for each_bear in ['PEP8Bear', 'PylintBear', 'VintBear']:
            if PipRequirement(each_bear).is_installed():
                PipRequirement(each_bear).uninstall_package()

    @patch('coala_package_manager.installation_tool.install_bears',
           return_value=['SomeBear'])
    def test_install_failing_bears_list(self, call_mock):
        self.argv += ['install', 'coalaBear']
        self.err_msg = 'Bears that failed installing/their dependencies failed'
        with retrieve_stderr() as stderr:
            main()
            self.assertIn(self.err_msg,
                          stderr.getvalue())
            self.assertIn('SomeBear',
                          stderr.getvalue())

    def test_list_bears(self):
        self.beg_line = 'This is a list of all the bears you can install'
        self.argv.append('list')
        with retrieve_stdout() as stdout:
            main()
            output = stdout.getvalue()
            self.assertIn(self.beg_line, output)
            output = output.split('\n')
            self.assertGreater(len(output), 50)

    def test_install_bears_without_dependency(self):
        self.argv += ['--ignore-deps', 'install', 'PEP8Bear', 'PylintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('PEP8Bear is installing', stdout.getvalue())
            self.assertIn('PyLintBear is installing', stdout.getvalue())
            self.assertIn('DONE!', stdout.getvalue())

    def test_install_invalid_bears(self):
        self.err_msg = 'The following bears are not bears'
        self.argv += ['install', 'TestBear']
        with self.assertRaises(SystemExit) as cm:
            with retrieve_stderr() as stderr:
                main()
                self.assertIn(self.err_msg, stderr.getvalue())
                self.assertIn('testbear', stderr.getvalue())

    def test_given_coafile_installation(self):
        self.my_file = "[foo]\nbears = VintBear\n"
        self.tempdir = tempfile.gettempdir()
        self.file = os.path.join(self.tempdir, '.coafile')
        with open(self.file, 'w') as file:
            file.write(self.my_file)

        self.argv += ['install', '-c', self.file]
        with retrieve_stdout() as stdout:
            main()
            self.assertTrue(PipRequirement('VintBear').is_installed())
        os.remove(self.file)

    def test_upgrade_not_installed_bears(self):
        self.err_msg = ('\nThe following bears were not installed and were '
                        'therefore not upgraded:\n')
        self.argv += ['upgrade', 'VintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.err_msg, stdout.getvalue())

    def test_upgrade_installed_bears(self):
        install_bears(['PEP8Bear', 'PylintBear'], True)
        self.argv += ['upgrade', 'PEP8Bear', 'PylintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('Upgrading PEP8Bear now..', stdout.getvalue())
            self.assertIn('Upgrading PyLintBear now..', stdout.getvalue())

    def test_uninstall_not_installed_bears(self):
        self.err_msg = ('\nThe following bears were not installed and were '
                        'therefore not uninstalled:\n')
        self.argv += ['uninstall', 'VintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.err_msg, stdout.getvalue())

    def test_uninstall_installed_bears(self):
        install_bears(['PEP8Bear', 'PyLintBear'], True)
        self.argv += ['uninstall', 'PEP8Bear', 'PylintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('Uninstalling PEP8Bear now..', stdout.getvalue())
            self.assertIn('Uninstalling PyLintBear now..', stdout.getvalue())

    def test_checkdep_not_installed_bears(self):
        self.err_msg = ('\nThe following bears were not installed and were '
                        'therefore not checked:\n')
        self.argv += ['check-deps', 'VintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn(self.err_msg, stdout.getvalue())

    def test_checkdep_installed_bears(self):
        install_bears(['PEP8Bear', 'PyLintBear'], True)
        self.argv += ['check-deps', 'PEP8Bear', 'PylintBear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('Checking PEP8Bear now..', stdout.getvalue())
            self.assertIn('Checking PyLintBear now..', stdout.getvalue())

    def test_show_not_installed_bear(self):
        self.argv += ['show', 'VintBear']
        self.err_msg = ('The following bears were not installed and '
                        'were therefore not shown')
        with retrieve_stderr() as stderr:
            main()
            self.assertIn(self.err_msg, stderr.getvalue())

    def test_show_installed_bear(self):
        install_bears(['PEP8Bear'], False)
        self.argv += ['show', 'PEP8Bear']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('Detects and fixes PEP8 incompliant code. This bear'
                          ' will not change functionality of the code in any'
                          ' way.\n', stdout.getvalue())

    def test_coafile_current_directory(self):
        self.my_file = "[foo]\nbears = VintBear\n"
        with open('.coafile', 'r+') as f:
            contents = f.read()
            f.truncate(0)
            f.write(self.my_file)
        self.argv += ['install', '-c']
        with retrieve_stdout() as stdout:
            main()
            self.assertTrue(PipRequirement('VintBear').is_installed())
        with open('.coafile', 'w') as f:
            f.write(contents)

    def test_invalid_command(self):
        self.argv += ['wrong_command']
        with self.assertRaises(SystemExit) as cm:
            main()
            self.assertEqual(cm.exception.code, 1)


class MainTestAllCommands(unittest.TestCase):

    def setUp(self):
        PipRequirement('PEP8Bear').install_package()
        self.argv = ['installation_tool.py']
        argv_patcher = patch.object(sys, 'argv', self.argv)
        self.addCleanup(argv_patcher.stop)
        self.argv_mock = argv_patcher.start()

    @patch('coala_package_manager.installation_tool.install_bears')
    def test_install_all(self, call_mock):
        self.argv += ['install', 'all', '--ignore-deps']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('we are installing all the bears right now.',
                          stdout.getvalue())
            self.assertGreater(len(call_mock.call_args[0][0]), 50)
            self.assertTrue(call_mock.call_args[0][0])

    @patch.object(PipRequirement, 'upgrade_package')
    def test_upgrade_all(self, call_mock):
        self.argv += ['upgrade', 'all']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('we are upgrading all the installed',
                          stdout.getvalue())

    @patch.object(PipRequirement, 'uninstall_package')
    def test_uninstall_all(self, call_mock):
        self.argv += ['uninstall', 'all']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('we are uninstalling all the installed',
                          stdout.getvalue())

    @patch('coala_package_manager.installation_tool.check_requirements')
    def test_check_deps_all(self, call_mock):
        self.argv += ['check-deps', 'all']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('we are checking all the installed',
                          stdout.getvalue())
            for call_object in call_mock.call_args_list:
                self.assertRegex(call_object[0][0], r'.+Bear')

    @patch('coala_package_manager.installation_tool.show_installed_bears')
    def test_show_all_installed_bears(self, call_mock):
        self.argv += ['show', 'all']
        with retrieve_stdout() as stdout:
            main()
            self.assertIn('showing all installed bears now...',
                          stdout.getvalue())
            self.assertGreater(len(call_mock.call_args[0][0]), 50)


def tearDownModule():
    if PipRequirement('autopep8').is_installed():
        PipRequirement('autopep8').uninstall_package()
    if PipRequirement('PEP8Bear').is_installed():
        PipRequirement('PEP8Bear').uninstall_package()
